SF_CONSUMER_KEY=3MVG9_XwsqeYoueLsSzmhitiAOZy6LORbZNJuWie3TGOJvaC9XSWTZBtbuB9muRhdUeyKH6NVN.nrJkiSFbal
SF_USERNAME=gauravtrivedi145@playful-otter-shwkmn.com
SCRATCH_ORG_ALIAS=Dream_Scratch
PACKAGE_NAME=DreamHouse_CI

# Authenticate to the Dev Hub using the server key
sfdx force:auth:jwt:grant --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME --setdefaultdevhubusername --setalias playful-otter
 ## Create scratch org

echo $PACKAGE_NAME
  ## Create packaged version
export PACKAGE_VERSION_ID="$(eval sfdx force:package:version:create --package $PACKAGE_NAME --installationkeybypass --wait 10 --json --targetdevhubusername playful-otter | jq '.result.SubscriberPackageVersionId' | tr -d '"')"
 ## Save your PACKAGE_VERSION_ID to a file for later use during deploy so you know what version to deploy
echo "$PACKAGE_VERSION_ID" > PACKAGE_VERSION_ID.TXT
echo $PACKAGE_VERSION_ID
 ## Install package in DevHub org (this is a compiled library of the app)
sfdx force:package:list
sfdx force:package:install --package $PACKAGE_VERSION_ID --wait 10 --publishwait 10 --targetusername $SCRATCH_ORG_ALIAS --noprompt
 ## Assign DreamHouse permission set to scratch org default user
sfdx force:user:permset:assign --targetusername $SCRATCH_ORG_ALIAS --permsetname DreamHouse
 ## Add sample data into app
sfdx force:data:tree:import --plan data/sample-data-plan.json --targetusername $SCRATCH_ORG_ALIAS
 ## Run unit tests in scratch org
sfdx force:apex:test:run --targetusername $SCRATCH_ORG_ALIAS --wait 10 --resultformat human --codecoverage --testlevel $TESTLEVEL
 ## Get the username for the scratch org
export SCRATCH_ORG_USERNAME="$(eval sfdx force:user:display --targetusername $SCRATCH_ORG_ALIAS --json | jq -r '.result.username')"
echo "$SCRATCH_ORG_USERNAME" > ./SCRATCH_ORG_USERNAME.TXT
 ## Generate a new password for the scrach org
sfdx force:user:password:generate --targetusername $SCRATCH_ORG_ALIAS
echo -e "\n\n\n\n"
 ## Display username, password, and instance URL for login
sfdx force:user:display --targetusername $SCRATCH_ORG_ALIAS
#